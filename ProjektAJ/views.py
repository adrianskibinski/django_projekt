from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.views.generic import View
from .models import Album, Song, Photo
from .forms import UserForm
from django.db.models import Q
from django.http import JsonResponse


class IndexView(generic.ListView):
    template_name = 'ProjektAJ/index.html'
    context_object_name = 'all_albums'

    def get_queryset(self):
        return Album.objects.all()


class DetailView(generic.DetailView):
    model = Album
    template_name = 'ProjektAJ/detail.html'


class AlbumCreate(CreateView):
    model = Album
    fields = ['artist', 'album_title', 'genre', 'album_logo']


class AlbumUpdate(UpdateView):
    model = Album
    fields = ['artist', 'album_title', 'genre', 'album_logo']


class AlbumDelete(DeleteView):
    model = Album
    success_url = reverse_lazy('ProjektAJ:index')


class SongCreate(CreateView):
    model = Song
    fields = ['album', 'file_type', 'song_title', 'audio_file', 'is_favorite']


class SongUpdate(UpdateView):
    model = Song
    fields = ['album', 'file_type', 'song_title', 'audio_file', 'is_favorite']


class SongDelete(DeleteView):
    model = Song
    fields = ['album', 'file_type', 'song_title', 'audio_file', 'is_favorite']


class PhotoCreate(CreateView):
    model = Photo
    fields = ['photo_album', 'file_type', 'photo_title', 'photo', 'is_favorite']


class PhotoUpdate(UpdateView):
    model = Photo
    fields = ['photo_album', 'file_type', 'photo_title', 'photo', 'is_favorite']


class PhotoDelete(DeleteView):
    model = Photo
    fields = ['photo_album', 'file_type', 'photo_title', 'photo', 'is_favorite']


class SongsView(generic.ListView):
    template_name = 'ProjektAJ/songs.html'
    context_object_name = 'all_songs'

    def get_queryset(self):
        return Song.objects.all()


class UserFormView(View):
    form_class = UserForm
    template_name = 'ProjektAJ/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:
                    login(request,user)
                    return redirect('ProjektAJ:index')

        return render(request, self.template_name, {'form': form})


#def songs(request):
#        users_songs = Song.objects.all()
#
#        return render(request, 'ProjektAJ/songs.html', {
#            'song_list': users_songs,
#        })


def searchview(request):
        albums = Album.objects.all()
#        song_results = Album.objects.all()
        query = request.GET.get("q")
        albums = albums.filter(
            Q(album_title__icontains=query) |
            Q(artist__icontains=query)
        ).distinct()
#        song_results = song_results.filter(
#            Q(song_title__icontains=query)
#        ).distinct()
        return render(request, 'ProjektAJ/search.html', {
            'albums': albums,
        })


def detail(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    return render(request, 'ProjektAJ/detail.html', {'album': album})


def favorite(request, song_id):
    song = get_object_or_404(Song, pk=song_id)
    try:
        if song.is_favorite:
            song.is_favorite = False
        else:
            song.is_favorite = True
        song.save()
    except (KeyError, Song.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})
