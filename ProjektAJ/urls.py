from django.conf.urls import url
from . import views

app_name = 'ProjektAJ'

urlpatterns = [

    # /ProjektAJ/
    url(r'^$', views.IndexView.as_view(), name='index'),

    # /ProjektAJ/<album_id>/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    # /ProjektAJ/album/add/
    url(r'album/add/$', views.AlbumCreate.as_view(), name='album-add'),

    # /ProjektAJ/album/2/
    url(r'album/(?P<pk>[0-9]+)/$', views.AlbumUpdate.as_view(), name='album-update'),

    # /ProjektAJ/album/2/delete/
    url(r'album/(?P<pk>[0-9]+)/delete/$', views.AlbumDelete.as_view(), name='album-delete'),

    # /ProjektAJ/register/
    url(r'^register/$', views.UserFormView.as_view(), name='register'),

    # /ProjektAJ/song/add/
    url(r'song/add/$', views.SongCreate.as_view(), name='song-add'),

    # /ProjektAJ/songs/
    url(r'^songs/(?P<filter_by>[a-zA_Z]+)/$', views.SongsView.as_view(), name='songs'),

    # /ProjektAJ/photo/add/
    url(r'photo/add/$', views.PhotoCreate.as_view(), name='photo-add'),

    # /ProjektAJ/search/
    url(r'^search/$', views.searchview, name='search'),

    # /ProjektAJ/<album_id>/
    url(r'^(?P<album_id>[0-9]+)/$', views.detail, name='detail'),

    # /ProjektAJ/<song_id>/favorite
    url(r'^(?P<song_id>[0-9]+)/favorite/$', views.favorite, name='favorite'),

]